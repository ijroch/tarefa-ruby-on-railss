Rails.application.routes.draw do
  resources :blogs
  root "wolves#index" #indica a rota que inicializa o site
  resources :wolves, path: 'lobos' 
  #get 'wolfes', to: 'wolves#index', as: 'new_lobinho' #criação de uma rota pro index
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
